<%-- 
    Document   : insertcandidat
    Created on : 29 sept. 2022, 09:38:53
    Author     : orlando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inscription Candidat</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col col-sm-2 col-sm-offset-1"></div>
                <div class="col col-sm-7">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="card-title text-center"> Inscription Candidat
                            </h2>
                        </div>
                        <div class="card-body">
                            <div class="container">
                                <form action="authen.do" method="post" class="form">
                                    <div class="mb-3 row">
                                        <label for="nomcandidat" class="col-4 col-form-label">Votre Ident</label>
                                        <div class="col-8">
                                            <input type="text" class="form-control" name="ident" id="nomcandidat" placeholder="Entre votre nom">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="prenomcandidat" class="col-4 col-form-label">Votre Prenom</label>
                                        <div class="col-8">
                                            <input type="text" class="form-control" name="motdepasse" id="prenomcandidat" placeholder="Entre votre prenom">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="offset-sm-4 col-sm-8">
                                            <input type="submit" class="form-control btn btn-primary mb-3" value="Enregistrer">
                                            <button type="reset" class="form-control btn btn-danger">Annule</button>

                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="card-footer text-muted">
                            Footer
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="js/script2.js"></script>

    </body>

</html>