/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import doframe.own.annotation.Attribute;
import doframe.own.annotation.PK;
import doframe.own.annotation.Tablename;
import dowebgen.annotation.Url;
import dowebgen.annotation.request.RequestBody;
import dowebgen.annotation.request.SessionParam;
import dowebgen.modelview.ModelView;
import service.ServiceAdmin;

/**
 *
 * @author orlando
 */
@Tablename("administrator")
public class Administrator {
    @PK
    @Attribute("idadmin")
    String id;
    @Attribute("identifiant")
    String ident;
    @Attribute
    String motdepasse;

    @Url("authenJSON.do")
    public ModelView authen(@RequestBody(name="q",reference=Administrator.class) Administrator ade) throws Exception{
        ModelView retour=new ModelView();
        
        Administrator ad=ServiceAdmin.authen(ade);
        if(ad==null){
            retour.setAttribute("message","Not Connected");
            retour.setUrldispatch("error.jsp");
            return retour;
        }else{
            retour.setAttribute("connect", ad);
            retour.setUrldispatch("connect.jsp");
        return retour;
        }
    } 
    
    @Url("authen.do")
    public ModelView authen() throws Exception{
        ModelView retour=new ModelView();
        Administrator ad=ServiceAdmin.authen(this);
        if(ad==null){
            retour.setAttribute("message","Not Connected");
            retour.setUrldispatch("error.jsp");
            return retour;
        }else{
            retour.setAttribute("connect", ad);
            User user=new User();
            user.setMotdepasse(this.getMotdepasse());
            retour.setSessionAttribute("user",user);
            retour.setUrldispatch("connect.jsp");
        return retour;
        }
    } 
    @Url("lasa.do")
    public ModelView TESTAUTHEN(@SessionParam(name="user") User user){
       ModelView retour=new ModelView();
       retour.setAttribute("us",user.getMotdepasse());
       retour.setUrldispatch("table.jsp");
       return retour;
    } 
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }
    
}
