/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import connection.Connectar;
import connection.Postgresql;
import doframe.own.generic.dao.Genericdao;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Administrator;

/**
 *
 * @author orlando
 */
public class ServiceAdmin {
    public static Administrator authen(Administrator admin) 
            throws Exception{
         Connectar con=new Connectar(new Postgresql("kilometer"),"societe","mdp",true);
         ArrayList<Object> obj=Genericdao.get(admin, true, con.getConnection());
         if(obj.size()==1){
             return (Administrator)obj.get(0);
         }
         return null;
    }
}
